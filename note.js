const response = {
    "data": {
        "message": "Upload image berhasil",
        "url": "http://res.cloudinary.com/yuneda/image/upload/v1654052866/jqozkxumwuat7uwf1qvh.jpg"
    },
    "status": 201,
    "statusText": "Created",
    "headers": {
        "content-length": "126",
        "content-type": "application/json; charset=utf-8"
    },
    "config": {
        "transitional": {
            "silentJSONParsing": true,
            "forcedJSONParsing": true,
            "clarifyTimeoutError": false
        },
        "transformRequest": [
            null
        ],
        "transformResponse": [
            null
        ],
        "timeout": 0,
        "xsrfCookieName": "XSRF-TOKEN",
        "xsrfHeaderName": "X-XSRF-TOKEN",
        "maxContentLength": -1,
        "maxBodyLength": -1,
        "env": {
            "FormData": null
        },
        "headers": {
            "Accept": "application/json, text/plain, */*"
        },
        "method": "put",
        "url": "http://localhost:8080/api/v1/profiles/1/picture/cloudinary",
        "data": {}
    },
    "request": {}
}